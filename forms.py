from flask.ext.wtf import Form
from wtforms.fields import SelectMultipleField, IntegerField, SubmitField
from wtforms.widgets.core import SubmitInput, Select

from rules import COST_RULES


class OrderForm(Form):
    goods = SelectMultipleField('Goods', choices=[(k, k) for k in COST_RULES], widget=Select(multiple=True))
    c_weight = IntegerField('Weight', default=0)
    add_to_basket = SubmitField('Add to basket', widget=SubmitInput('submit'))
    order = SubmitField('Order', widget=SubmitInput('submit'))