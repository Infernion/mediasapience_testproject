import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
db = SQLAlchemy(app)


class OrderModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text)
    items = db.Column(db.Text)
    date = db.Column(db.DATETIME)
    total_price = db.Column(db.Numeric)

    def __init__(self, items, total_price, username):
        self.items = items
        self.total_price = total_price
        self.date = datetime.datetime.now()
        self.username = username