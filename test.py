import unittest
from rules import COST_RULES, WEIGHT_RULES
from app import Checkout


class TestCheckout(unittest.TestCase):

    def price(self, goods, weight=None):
        co = Checkout(COST_RULES)
        for item in list(goods):
            co.scan(item, weight)
        return co.total, sorted(co.cart)

    def test_scan(self):
        self.assertEqual(0, self.price("")[0])
        self.assertEqual(50, self.price("A")[0])
        self.assertEqual(80, self.price("AB")[0])
        self.assertEqual(268.655, self.price("CDBA", weight=345)[0])

        self.assertEqual(100, self.price("AA")[0])
        self.assertEqual(130, self.price("AAA")[0])
        self.assertEqual(180, self.price("AAAA")[0])
        self.assertEqual(230, self.price("AAAAA")[0])
        self.assertEqual(260, self.price("AAAAAA")[0])

        self.assertEqual(160, self.price("AAAB")[0])
        self.assertEqual(175, self.price("AAABB")[0])
        self.assertEqual(295, self.price("AAABBD")[0])
        self.assertEqual(295, self.price("DABABA")[0])

        # Test to adding gift
        self.assertEqual((415, "A A A B B D D E(gift)".split()),
                         self.price("DDABABA"))
        self.assertEqual((535, "A A A B B D D D E(gift)".split()),
                         self.price("DDABDABA"))

        # Teste com itens inexistentes
        self.assertEqual(0, self.price("Z")[0])
        self.assertEqual(50, self.price("ZA")[0])