import random
from flask import Flask, render_template, session, redirect, url_for

from forms import OrderForm
from models import OrderModel, db
from rules import COST_RULES, WEIGHT_RULES

app = Flask(__name__)


class Checkout(object):
    # Scan the product and adds to the total shopping cart
    def scan(self, item, weight=None):

        if item not in self.rules:
            return

        self.cart.append(item)

        price = self.rules[item]["price"]
        special_price = self.rules[item]["special_price"]
        special_qtd = self.rules[item]["special_qtd"]
        unit = self.rules[item]["unit"]
        item_free = self.rules[item]["item_free"]

        # Add gift to cart
        if item_free and self.cart.count(item) % special_qtd == 0:
            self.cart.append('{}(gift)'.format(item_free))

        # Verify if the product has a discout price
        if special_price and self.cart.count(item) % special_qtd == 0:
            price = special_price - ((special_qtd - 1) * price)
        elif unit and weight:
            price = price / WEIGHT_RULES[unit] * weight
        else:
            price = price
        # Add the price to the total
        self.total += price

    def __init__(self, rules):
        self.rules = rules
        self.cart = []
        self.total = 0


@app.route('/', methods=["GET", "POST"])
def order():
    form = OrderForm()
    co = Checkout(COST_RULES)

    if not session.get('goods'):
        session['goods'] = {
            'cart': [],
            'total': 0,
        }

    if form.validate_on_submit() and form.add_to_basket.data:
        goods = form.data['goods']
        weight = form.data.get('c_weight')
        if session['goods']['cart']:
            co.cart = session['goods']['cart']

        for item in goods:
            co.scan(item, weight)
            session['goods']['total'] += co.total
        session['goods']['cart'] = co.cart

    elif form.validate_on_submit() and form.order:
        order = OrderModel(username='Testuser',
                           items=', '.join(session['goods']['cart']),
                           total_price=session['goods']['total'])
        db.session.add(order)
        db.session.commit()
        del session['goods']
        return render_template('success.html', order=order)
    return render_template('order.html', form=form)


if __name__ == '__main__':
    # app.debug = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test3.db'
    app.config['SECRET_KEY'] = 'ssra43465h  sdgsrdh3!@#wfst.db'
    db.create_all()
    app.run(port=8080)
