COST_RULES = {
    "A": {"price": 50,  "special_price": 130,  "special_qtd": 3,    "unit": None, "item_free": None},
    "B": {"price": 30,  "special_price": 45,   "special_qtd": 2,    "unit": None, "item_free": None},
    "C": {"price": 199, "special_price": None, "special_qtd": None, "unit": "kg", "item_free": None},
    "D": {"price": 120, "special_price": None, "special_qtd": 2,    "unit": None, "item_free": "E"},
    "E": {"price": 90,  "special_price": None, "special_qtd": None, "unit": None, "item_free": None},
}

WEIGHT_RULES = {
    "kg": 1000
}
